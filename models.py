from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime
from sqlalchemy.orm import relationship
from database import Base
from db_models.helpers import CleanRepr


class Metasource(Base, CleanRepr):
    __tablename__ = 'Metasources'
    id = Column(Integer, primary_key=True)
    title = Column(String(30), nullable=False)
    url = Column(String(20), nullable=False)
    search_by = Column(String(20), nullable=True)
    search = Column(String(30), nullable=True)


class Source(Base, CleanRepr):
    __tablename__ = 'Sources'
    id = Column(Integer, primary_key=True)
    title = Column(String(30), nullable=False)
    category = Column(String(30), nullable=False)
    rss = Column(String(120), nullable=False)
    parent = Column(Integer, ForeignKey('Metasources.id'))
    Metasources = relationship(Metasource)


class Entry(Base, CleanRepr):
    __tablename__ = 'Entries'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime, nullable=False)
    title = Column(String(200), nullable=False)
    summary = Column(String(2000), nullable=True)  # nullable==True couse news body may be missing
    text = Column(String(10000), nullable=True)
    link = Column(String(200), nullable=False)
    processed = Column(Boolean, nullable=False)
    source = Column(Integer, ForeignKey('Sources.id'))
    Sources = relationship(Source)
