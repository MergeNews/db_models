class CleanRepr():
    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__,
                                 {k: v for k, v in self.__dict__.items()
                                  if not k.startswith('_')}.values())
